use crate::serides::seri::{Serialize, nb};
use super::{GraphicsContext, GCOptions};

pub(crate) struct CreateGC {
    id: u32,
    drawable: u32,
    options: GCOptions,
}

impl CreateGC {
    pub(crate) fn new(gc: &GraphicsContext, options: GCOptions) -> Self {
        CreateGC {
            id: gc.id,
            drawable: gc.drawable,
            options: options,
        }
    }
}

impl Serialize for CreateGC {
    fn serialize_to(&self, buf: &mut Vec<u8>) {
        nb!(buf, 55u8); // CreateGC opcode
        nb!(buf, 0u8); // unused
        nb!(buf, (self.seri_bufsize() / 4) as u16); // request length: 4 + number of values
        nb!(buf, self.id); // id for new context
        nb!(buf, self.drawable); // id of drawable to pull root + depth from
        nb!(buf, self.options.mask()); // value mask

        for value in self.options.values() {
            nb!(buf, value);
        }
    }

    fn seri_bufsize(&self) -> usize {
        (4 + self.options.hash.len()) * 4
    }
}
