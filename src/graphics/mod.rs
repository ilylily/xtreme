mod messages;

use std::sync::Arc;

use crate::display::XDisplay;
use crate::window::Window;
use crate::serides::seri::{Serialize, nb};
use crate::serides::bitmask_hash;

pub trait Drawable {
    fn id(self) -> u32;
}

impl Drawable for &Window {
    fn id(self) -> u32 {
        (&self).id()
    }
}

pub struct GraphicsContext {
    display: Arc<XDisplay>,
    id: u32,
    drawable: u32,
}

bitmask_hash!{
    pub bitmask GCOptions GCOptionsKey GC_OPTION_KEYS_COUNT {
        Function,
        PlaneMask,
        Foreground,
        Background,
        LineWidth,
        LineStyle,
        CapStyle,
        JoinStyle,
        FillStyle,
        FillRule,
        Tile,
        Stipple,
        TileStippleXOrigin,
        TileStippleYOrigin,
        Font,
        SubwindowMode,
        GraphicsExposures,
        ClipXOrigin,
        ClipYOrigin,
        ClipMask,
        DashOffset,
        Dashes,
        ArcMode,
    }
}

impl GraphicsContext {
    pub fn create(display: &Arc<XDisplay>, drawable: impl Drawable, options: GCOptions) -> GraphicsContext {
        let display = display;
        let did = drawable.id();
        let cid = display.generate_res_id();
        let gc = GraphicsContext {
            display: display.clone(),
            id: cid,
            drawable: did,
        };

        let msg = messages::CreateGC::new(&gc, options);
        let buf = msg.serialize();
        let socket_ref = display;
        let mut socket = socket_ref.socket.write().unwrap();
        socket.write_all(&buf[..]).unwrap();
        socket.flush().unwrap();

        return gc;
    }

    pub fn rectangle(&self, x: i16, y: i16, w: u16, h: u16) {
        let display = self.display.clone();
        let mut socket = display.socket.write().unwrap();

        let mut buf = Vec::with_capacity(4*5);
        nb!(buf, 67u8);
        nb!(buf, 0u8);
        nb!(buf, 5u16);
        nb!(buf, self.drawable);
        nb!(buf, self.id);
        nb!(buf, x);
        nb!(buf, y);
        nb!(buf, w);
        nb!(buf, h);
        socket.write_all(&buf[..]).unwrap();
        socket.flush().unwrap();
    }

    pub fn line(&self, start_x: i16, start_y: i16, end_x: i16, end_y: i16) {
        let display = self.display.clone();
        let mut socket = display.socket.write().unwrap();

        let mut buf = Vec::with_capacity(5*4);
        nb!(buf, 66u8); // PolySegments opcode
        nb!(buf, 0u8);
        nb!(buf, 5u16);
        nb!(buf, self.drawable);
        nb!(buf, self.id);
        nb!(buf, start_x);
        nb!(buf, start_y);
        nb!(buf, end_x);
        nb!(buf, end_y);
        socket.write_all(&buf[..]).unwrap();
        socket.flush().unwrap();
    }

    /// render text at 15, 10. panics if text longer than 254 bytes
    pub fn text(&self, text: &str) {
        // TODO: rewrite to be like, useful
        let display = self.display.clone();
        let mut socket = display.socket.write().unwrap();

        assert!(text.len() < 255);
        let text_len: u8 = text.len().try_into().unwrap();
        let mut padding_len = 0;
        while (2 + text_len + padding_len) % 4 != 0 {
            padding_len += 1;
        }
        println!("2 + {} + {} = {}", text_len, padding_len, 2 + text_len + padding_len);

        let mut buf = Vec::with_capacity(4 + text.len() / 4);
        nb!(buf, 74u8); // 1
        nb!(buf, 0u8); // 1
        nb!(buf, (4 + (2 + text_len + padding_len) / 4) as u16); // 2
        nb!(buf, self.drawable); // 4
        nb!(buf, self.id); // 4
        nb!(buf, 15i16); // 2
        nb!(buf, 10i16); // 2

        // textitems (we only have one)
        nb!(buf, text_len);
        nb!(buf, 0u8); // delta
        buf.extend_from_slice(text.as_bytes());
        for _ in 0..padding_len {
            nb!(buf, 0u8);
        }
        //panic!("{} ({}): {:?}", buf.len(), buf.len() as f32 / 4.0, buf);
        socket.write_all(&buf[..]).unwrap();
        socket.flush().unwrap();
    }
}
