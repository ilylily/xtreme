use std::str::FromStr;
use std::path::Path;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;
use std::net::TcpStream;
use std::io;
use std::io::{Read, Write};
use std::fmt::Formatter;
use std::fmt::Debug;
use std::sync::{Arc, Weak, RwLock, Mutex};

#[cfg(feature = "unix")]
use std::os::unix::net::UnixStream;

mod server_info;
use server_info::ServerInfo;

use crate::serides::des::deserialize;
use crate::serides::seri::nb;

/// represents a connection to an X server
#[derive(Debug)]
pub struct XDisplay {
    pub(crate) handle: Weak<XDisplay>,
    pub(crate) socket: RwLock<Box<dyn XConnection>>,

    /// this will be None before connection is initialized
    pub server_info: Option<ServerInfo>,

    // seed/state for generating resource ids
    rand_state: Mutex<u32>,
}

/// read the environment variable `DISPLAY` and return the appropriate `XDisplay`
pub fn open() -> Result<Arc<XDisplay>, OpenError> {
    let display: DisplaySpecifier = match std::env::var("DISPLAY") {
        Ok(s) => DisplaySpecifier::from_str(&s)?,
        Err(std::env::VarError::NotPresent) => DisplaySpecifier::from_str(":0")?,
        Err(e) => return Err(e)?,
    };

    return Ok(open_connection(display)?)
}

/// create an XDisplay based on the passed `DISPLAY`-style argument
pub fn open_display(display: &str) -> Result<Arc<XDisplay>, OpenError> {
    let display = match DisplaySpecifier::from_str(display) {
        Ok(spec) => spec,
        Err(e) => return Err(e)?,
    };
    return Ok(open_connection(display)?)
}

/// opens a connection according to the given `DisplaySpecifier`, or None
fn open_connection(display: DisplaySpecifier) -> Result<Arc<XDisplay>, ConnectError>  {
    let mut socket: Box<dyn XConnection> = match &display.address {
        DisplayAddress::Tcp(a) => Box::new(match TcpStream::connect(a) {
            Ok(sock) => sock,
            Err(e) => return Err(e)?,
        }),
        #[cfg(feature = "unix")]
        DisplayAddress::Unix(p) => Box::new(match UnixStream::connect(p) {
            Ok(sock) => sock,
            Err(e) => return Err(e)?,
        }),
    };

    let xauthority = crate::auth::read_auth();

    // sized to fit 'MIT-MAGIC-COOKIE-1' or 'XDM-AUTHORIZATION-1', which are the two types
    // i see in my ~/.Xauthority
    // check yours with this pretty little number:
    // printf 'f = open("%s", "rb"); short = lambda f: int.from_bytes(f.read(2), "big"); string = lambda f: f.read(short(f)); proto = lambda f: (f.read(2) + string(f) + string(f)) * 0 + string(f) + string(f) * 0; print(set([ck for ck in iter(lambda: proto(f), b"")])); f.close();' ~/.Xauthority | python3 -
    let mut auth_protocol = Vec::with_capacity(24);
    let mut auth_data = Vec::with_capacity(24);

    // TODO: break this out into auth. this is auth's job
    let family = display.address.to_family();
    for token in xauthority {
        // accept unix-type tokens if tcp address is local
        if (family != token.family) && (family == 0 && !socket.is_local()) { continue; }
        if display.number != token.number { continue; }
        auth_protocol.extend_from_slice(&token.name);
        auth_data.extend_from_slice(&token.data);
        break; // TODO: find ideal token and break after best match
    }

    let server_info = match hello(&mut socket, &auth_protocol, &auth_data) {
        Ok(data) => {
            Some(ServerInfo::from(data))
        },
        Err(HelloError::IO(e)) if e.kind() == io::ErrorKind::UnexpectedEof => {
            if auth_protocol.len() == 0 {
                // if the server stopped responding or closed the connection, it might be because
                // we couldn't find an appropriate auth token
                return Err(io::Error::new(
                        io::ErrorKind::UnexpectedEof,
                        "no appropriate auth token was found, and server didn't reply to empty auth attempt"
                    )
                )?
            }
            return Err(e)?
        },
        Err(e) => return Err(e)?,
    };

    let rand_state = {
        let now = std::time::SystemTime::now();
        let duration = now.duration_since(std::time::SystemTime::UNIX_EPOCH);
        match duration {
            Err(_) => 1_234_567_890,
            Ok(duration) => (duration.as_nanos() % 0xffff_ffff) as u32,
        }
    };

    let xdisplay = Arc::new_cyclic(|me| {
        XDisplay {
        handle: me.clone(),
        socket: RwLock::new(socket),
        server_info,
        rand_state: Mutex::new(rand_state),
    }});

    Ok(xdisplay.handle.upgrade().unwrap())
}

crate::error_macros::wrapper_err!{
    /// an error while parsing a `DISPLAY`-style specifier string, or while resolving the target
    /// address
    pub enum SpecifierError
    {
        /// wraps [std::num::ParseIntError], in case of a problem parsing the display number or
        /// Xinerama monitor specifier
        ParseInt(std::num::ParseIntError),

        /// in the case of a tcp connection, couldn't resolve the hostname part of the specifier
        Unresolvable,
        /// if there's no colon in a display specifier, it's impossible to tell what connection
        /// method to use, or even what x display to connect to
        MissingColon,
    }
}

crate::error_macros::wrapper_err!{
    /// an error in the [open] or [open_display] methods which prevents an [XDisplay] from being
    /// returned
    pub enum OpenError
    {
        /// for when there was an error reading the `DISPLAY` environent variable
        Var(std::env::VarError),

        /// a [ConnectError]. couldn't establish the connection, or maybe perform initial setup
        Connect(ConnectError),
        /// a [SpecifierError]. the `DISPLAY`-style specifier string was invalid or there's a
        /// parser bug
        Specifier(SpecifierError),
    }
}

crate::error_macros::wrapper_err!{
    /// an error opening the connection to the x server, or else during initial setup
    pub enum ConnectError {
        /// for issues performing the actual socket connections, reads, writes, etc.
        IO(io::Error),

        /// a [HelloError]. couldn't complete the initial handshake with the x server
        Hello(HelloError)
}}

#[derive(Debug)]
/// for when the x server returns an error during client handshake
pub enum HelloError {
    /// couldn't read or write bytes on open socket; connection probably closed unexpectedly
    IO(io::Error),

    /// failed to setup initial connection. this includes the protocol version and a reason
    Failure{version_major: u16, version_minor: u16, reason: String},
    /// failed to authenticate. this also includes a reason
    Authenticate{reason: String},
}

struct DisplaySpecifier {
    number: Vec::<u8>,
    address: DisplayAddress,
}

enum DisplayAddress {
    Tcp(SocketAddr),
    #[cfg(feature = "unix")]
    Unix(Box<Path>),
}

impl DisplayAddress {
    /// return the type of this specifier as an Xauthority "family" type. this is a 16-bit unsigned
    /// int where 0 means a tcp connection and 256 means a local connection. i have no examples of
    /// any other family types and don't wanna dig through X11 headers to find them. read
    /// XSecurity(7) for more info
    fn to_family(&self) -> u16 {
        match self {
            Self::Tcp(_) => 0u16,
            #[cfg(feature = "unix")]
            Self::Unix(_) => 256u16,
        }
    }
}

pub(crate) trait XConnection: Read + Write {
    fn remote(&self) -> Result<String, io::Error>;
    fn is_local(&self) -> bool;
}

#[cfg(feature = "unix")]
impl XConnection for UnixStream {
    fn remote(&self) -> Result<String, io::Error> {
        return Ok(format!("{:?}", self.peer_addr()?));
    }

    fn is_local(&self) -> bool {
        true
    }
}

impl XConnection for TcpStream {
    fn remote(&self) -> Result<String, io::Error> {
        return Ok(format!("{:?}", self.peer_addr()?));
    }

    /// TODO: does this actually guarantee that the connection is local?
    fn is_local(&self) -> bool {
        self.local_addr().unwrap().ip() == self.peer_addr().unwrap().ip()
    }
}

impl Debug for dyn XConnection {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.debug_struct("XConnection").field("remote", &self.remote().unwrap()).finish()
    }
}

impl FromStr for DisplaySpecifier {
    type Err = SpecifierError;

    /// take a string formatted like the X11 `DISPLAY` environment variable and return the thing it
    /// should connect to.
    fn from_str(display: &str) -> Result<DisplaySpecifier, SpecifierError> {
        // TODO: handle multiheaded displays instead of throwing away everything after the dot (lmao)
        let colon = match display.find(':') {
            Some(idx) => idx,
            None => return Err(SpecifierError::MissingColon),
        };

        let dot = match display.rfind('.') {
            // only chop the last dot if it's after the colon (like `localhost:0.0`, not `127.0.0.1:0`)
            Some(i) => if i > colon { i } else { display.len() },
            None => display.len(),
        };

        match colon {
            #[cfg(feature = "unix")]
            0 => {
                // TODO: handle multihead display specifications
                let display_number = &display[1..dot];
                let path = Path::new("/tmp/.X11-unix").join(&format!("X{}", display_number));
                let path = Box::<Path>::from(path);

                Ok(DisplaySpecifier {
                    number: Vec::from(display_number),
                    address: DisplayAddress::Unix(path)
                })
            },

            #[cfg(not(feature = "unix"))]
            0 => unimplemented!("no local socket support on non-unix platforms. use tcp"),

            _ => {
                // TODO: check something to see if it's appropriate to add 6000?
                let display_host = &display[0..colon];
                let display_number = &display[colon+1..dot];
                let display_port = match display_number.parse::<u16>() {
                    Ok(i) => i + 6000,
                    Err(e) => return Err(e)?,
                };
                if let Ok(addrs) = (display_host, display_port).to_socket_addrs() {
                    for addr in addrs {
                        // TODO: handle multiple address resolutions?
                        return Ok(DisplaySpecifier {
                            number: Vec::from(display_number),
                            address: DisplayAddress::Tcp(addr)
                        })
                    }
                }

                Err(SpecifierError::Unresolvable)
            },
        }
    }
}

enum X11ByteOrder {
    BigEndian,
    LittleEndian,
}

impl From<&X11ByteOrder> for u8 {
    fn from(bo: &X11ByteOrder) -> u8 {
        match bo {
            X11ByteOrder::BigEndian => 0x42,
            X11ByteOrder::LittleEndian => 0x6c,
        }
    }
}

struct ConnectionSetupRequest {
    byte_order: X11ByteOrder,
    auth_protocol: Vec<u8>,
    auth_data: Vec<u8>,
}

impl ConnectionSetupRequest {
    fn serialize(&self) -> Vec::<u8> {
        let padding = [0u8; 4];

        // TODO: don't unwrap. but if auth proto or data len is huge that's weird
        let auth_proto_len: u16 = self.auth_protocol.len().try_into()
            .expect("auth protocol name is too long - probably a bug in the Xauthority parser");
        let auth_data_len: u16 = self.auth_data.len().try_into()
            .expect("auth token data is too long - probably a bug in the Xauthority parser");
        let mut buffer = Vec::with_capacity(12 + auth_proto_len as usize + auth_data_len as usize + 8);

        nb!(buffer, u8::from(&self.byte_order)); // byte order marker
        nb!(buffer, 0u8); // unused

        // hi, we're X11 version uh... 11. (this works!!)
        nb!(buffer, 11u16); // major version (always 11)
        nb!(buffer, 00u16); // minor version (always zero)

        nb!(buffer, auth_proto_len); // auth protocol name len
        nb!(buffer, auth_data_len); // auth protocol name padding
        nb!(buffer, 0x0000u16); // unused

        buffer.extend_from_slice(&self.auth_protocol); // auth protocol name
        if auth_proto_len % 4 > 0 {
            buffer.extend_from_slice(&padding[0..auth_proto_len as usize % 4]);
        }

        buffer.extend_from_slice(&self.auth_data); // auth protocol data
        if auth_data_len % 4 > 0 {
            buffer.extend_from_slice(&padding[0..auth_data_len as usize % 4]);
        }

        return buffer;
    }
}

fn hello(socket: &mut Box<dyn XConnection>, auth_protocol: &Vec<u8>, auth_data: &Vec<u8>) -> Result<Vec<u8>, HelloError> {
    let request = ConnectionSetupRequest {
        byte_order: X11ByteOrder::LittleEndian,
        auth_protocol: auth_protocol.clone(),
        auth_data: auth_data.clone(),
    };

    let buffer = request.serialize();
    if let Err(e) = socket.write_all(&buffer[..]) {
        return Err(HelloError::IO(e));
    }

    let mut buffer = [0u8; 1];
    if let Err(e) = socket.read_exact(&mut buffer) {
        return Err(HelloError::IO(e));
    }
    let success: u8 = buffer[0];

    // TODO: read other response fields
    match success {
        0 => {
            deserialize!(
                socket,
                reason_len: u8,
                version_major: u16,
                version_minor: u16,
                padding_len: u16,
                reason: String = reason_len,
                _pad: String = padding_len * 4 - reason_len as u16 // discard padding. TODO: consider validating
            );

            Err(HelloError::Failure{
                version_major,
                version_minor,
                reason,
            })
        },
        1 => {
            deserialize!(
                socket,
                _unused: u8,
                _version_major: u16,
                _version_minor: u16,
                data_len: u16,
                data: Vec::<u8> = data_len * 4
            );
            Ok(data)
        },
        2 => Err(HelloError::Authenticate{reason: "TODO".to_string()}),
        other => panic!("x server returned unknown handshake response success byte '{}'", other),
    }
}

impl XDisplay {
    /// TODO: i don't know if we can meaningfully close an arc<t>
    pub fn close(&self) {
        println!("{:?}", self.socket);
        //drop(self);
        // TODO: todo!("sync and report errors?")
    }

    /// TODO: `Display`-esque, so just implement `Display` or `Debug` or whatever one is supposed
    /// to implement for such things. returns a String currently just conaining the output of the
    /// backing socket's `peer_addr` function
    pub fn remote(&self) -> Result<String, io::Error> {
        self.socket.read().unwrap().remote()
    }

    /// generate an identifier for a window, pixmap, cursor, font, gcontext, or colourmap. this is
    /// the lehmer rng from wikipedia. https://en.wikipedia.org/wiki/Lehmer_random_number_generator
    pub(crate) fn generate_res_id(&self) -> u32 {
        let mut rand_state = self.rand_state.lock().unwrap();
        *rand_state = ((*rand_state as u64) * 48271 % 0x7fffffff) as u32;

        let info = self.server_info.as_ref().unwrap();
        return *rand_state & info.resource_id_mask | info.resource_id_base;
    }
}
