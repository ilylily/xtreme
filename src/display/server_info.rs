use crate::serides::des::deserialize_slice;

/// represents server info as returned during connection initiation
pub struct ServerInfo {
    //pub version_major: u16,
    //pub version_minor: u16,
    pub vendor: String,
    pub release_number: u32,
    pub(crate) resource_id_base: u32,
    pub(crate) resource_id_mask: u32,
    image_byte_order: ImageByteOrder,
    bitmap_scanline_unit: u8, // one of 8, 16, 32
    bitmap_scanline_pad: u8, // one of 8, 16, 32
    bitmap_bit_order: BitmapBitOrder,
    pixmap_formats: Vec<PixmapFormat>,
    roots: Vec<Screen>,
    motion_buffer_size: u32,
    maximum_request_length: u16,
    min_keycode: KeyCode,
    max_keycode: KeyCode,
}

impl std::fmt::Debug for ServerInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "X{}.{} server by {}, release number {}",
            11, 0,
            self.vendor,
            self.release_number,
        )
    }
}

impl From<Vec::<u8>> for ServerInfo {
    fn from(data: Vec::<u8>) -> ServerInfo {
        // magic: for the deserialization macro. keeps place
        let mut cursor: usize = 0;

        deserialize_slice!(&data, cursor,
            release_number: u32,
            resource_id_base: u32,
            resource_id_mask: u32,
            motion_buffer_size: u32,
            vendor_len: u16,
            maximum_request_length: u16,
            roots_len: u8, // number of `Screen`s in `roots`
            formats_len: u8, // number of `PixmapFormat`s in `pixmap_formats`
            image_byte_order: u8,
            bitmap_bit_order: u8, // also called bitmap-format-bit-order in protocol example??
            bitmap_scanline_unit: u8, // these two also get the bitmap-format treatment
            bitmap_scanline_pad: u8, // ... weird
            min_keycode: u8,
            max_keycode: u8,
            _unused: u32,
            vendor: String = vendor_len,
            _pad: String = (4 - (vendor_len % 4)) % 4
        );

        let mut pixmap_formats = Vec::<PixmapFormat>::with_capacity(formats_len as usize);
        for _ in 0..formats_len {
            deserialize_slice!(data, cursor,
                depth: u8,
                bits_per_pixel: u8,
                scanline_pad: u8,
                _unused: u8,
                _unused: u32
            );
            let fmt = PixmapFormat {
                depth, bits_per_pixel, scanline_pad
            };
            pixmap_formats.push(fmt);
        }

        // XXX: i did algebra for this :(
        /*
        let roots_size = (data_len as usize - 8 - 2 * formats_len as usize) * 4 - vendor_len as usize - _pad.capacity();
        let mut buffer = Vec::with_capacity(roots_size);
        buffer.resize(roots_size, 0u8);
        stream.read_exact(&mut buffer[..]).unwrap();
        */

        let mut roots = Vec::<Screen>::new();
        for _ in 0..roots_len {
            deserialize_slice!(data, cursor,
                window_id: u32,
                default_colourmap: u32,
                white_pixel: u32,
                black_pixel: u32,
                current_input_masks: u32,
                width_in_pixels: u16,
                height_in_pixels: u16,
                width_in_millimetres: u16,
                height_in_millimetres: u16,
                min_installed_maps: u16,
                max_installed_maps: u16,
                root_visual: u32,
                backing_stores: u8,
                save_unders: u8,
                root_depth: u8,
                allowed_depths_len: u8
            );

            let save_unders = match save_unders {
                0 => false,
                1 => true,
                _ => panic!("while parsing save_unders: that's no bool")
            };
            let backing_stores = backing_stores.try_into().unwrap();
            let current_input_masks = current_input_masks.try_into().unwrap();

            let mut allowed_depths = Vec::with_capacity(allowed_depths_len as usize);
            for _ in 0..allowed_depths_len {
                deserialize_slice!(data, cursor,
                    depth: u8,
                    _unused: u8,
                    visuals_len: u16,
                    _unused: u32
                );
                let mut visuals = Vec::with_capacity(visuals_len as usize);
                for _ in 0..visuals_len {
                    deserialize_slice!(data, cursor,
                        visual_id: u32,
                        class: u8,
                        bits_per_rgb_value: u8,
                        colourmap_entries: u16,
                        red_mask: u32,
                        green_mask: u32,
                        blue_mask: u32,
                        _unused: u32
                    );
                    let class = class.try_into().unwrap();
                    visuals.push(VisualType {
                        visual_id,
                        class,
                        bits_per_rgb_value,
                        colourmap_entries,
                        red_mask,
                        green_mask,
                        blue_mask
                    });
                }
                allowed_depths.push(ScreenDepth {
                    depth,
                    visuals
                });
            }

            let root = window_id;
            //let root = crate::window::from_id(display.handle.clone(), window_id);

            roots.push(Screen {
                root,
                width_in_pixels,
                height_in_pixels,
                width_in_millimetres,
                height_in_millimetres,
                allowed_depths,
                root_depth,
                root_visual,
                default_colourmap,
                white_pixel,
                black_pixel,
                min_installed_maps,
                max_installed_maps,
                backing_stores,
                save_unders,
                current_input_masks,
            });
        }

        let image_byte_order = match image_byte_order {
            0 => ImageByteOrder::LSBFirst,
            1 => ImageByteOrder::MSBFirst,
            _ => todo!("handle invalid image-byte-order"),
        };

        let bitmap_bit_order = match bitmap_bit_order {
            0 => BitmapBitOrder::LeastSignificant,
            1 => BitmapBitOrder::MostSignificant,
            _ => todo!("handle invalid bitmap-bit-order"),
        };

        let min_keycode = min_keycode as KeyCode;
        let max_keycode = max_keycode as KeyCode;

        return ServerInfo {
            //version_major,
            //version_minor,
            vendor,
            release_number,
            resource_id_base,
            resource_id_mask,
            image_byte_order,
            bitmap_scanline_unit,
            bitmap_scanline_pad,
            bitmap_bit_order,
            pixmap_formats,
            roots,
            motion_buffer_size,
            maximum_request_length,
            min_keycode,
            max_keycode,
        }
    }
}

impl ServerInfo {

    pub fn get_screen(&self, index: u8) -> Option<&Screen> {
        return self.roots.get(index as usize)
    }
}

#[derive(Debug)]
enum ImageByteOrder {
    LSBFirst,
    MSBFirst,
}

#[derive(Debug)]
enum BitmapBitOrder {
    LeastSignificant,
    MostSignificant,
}

struct PixmapFormat {
    depth: u8,
    bits_per_pixel: u8, // one of 1, 4, 8, 16, 24, 32
    scanline_pad: u8, // one of 8, 16, 32
}

pub struct Screen {
    //pub root: crate::window::Window,
    pub root: u32,
    width_in_pixels: u16,
    height_in_pixels: u16,
    width_in_millimetres: u16,
    height_in_millimetres: u16,
    allowed_depths: Vec<ScreenDepth>,
    root_depth: u8,
    root_visual: VisualId,
    default_colourmap: ColourMap,
    white_pixel: u32,
    black_pixel: u32,
    min_installed_maps: u16,
    max_installed_maps: u16,
    backing_stores: BackingStores,
    save_unders: bool,
    current_input_masks: SetOfEvent,
}

struct ScreenDepth {
    depth: u8,
    visuals: Vec<VisualType>,
}

struct VisualType {
    visual_id: VisualId,
    class: VisualTypeClass,
    red_mask: u32,
    green_mask: u32,
    blue_mask: u32,
    bits_per_rgb_value: u8,
    colourmap_entries: u16,
}

// simply a 32-bit value, with the top 3 bits guaranteed to be zero
type VisualId = u32;

#[derive(Debug)]
enum VisualTypeClass {
    StaticGray,
    StaticColor,
    TrueColor,
    GrayScale,
    PseudoColor,
    DirectColor,
}

impl TryFrom<u8> for VisualTypeClass {
    type Error = ();

    fn try_from(i: u8) -> Result<Self, Self::Error> {
        match i {
            0 => Ok(VisualTypeClass::StaticGray),
            1 => Ok(VisualTypeClass::GrayScale),
            2 => Ok(VisualTypeClass::StaticColor),
            3 => Ok(VisualTypeClass::PseudoColor),
            4 => Ok(VisualTypeClass::TrueColor),
            5 => Ok(VisualTypeClass::DirectColor),
            _ => Err(())
        }
    }
}

// simply a 32-bit value, with the top 3 bits guaranteed to be zero
type ColourMap = u32;

#[derive(Debug)]
enum BackingStores {
    Never,
    WhenMapped,
    Always,
}

impl TryFrom<u8> for BackingStores {
    type Error = ();

    fn try_from(i: u8) -> Result<Self, Self::Error> {
        match i {
            0 => Ok(BackingStores::Never),
            1 => Ok(BackingStores::WhenMapped),
            2 => Ok(BackingStores::Always),
            _ => Err(())
        }
    }
}

/// TODO: move event stuff to somewhere more general
struct SetOfEvent {
    set: u32, 
}

impl TryFrom<u32> for SetOfEvent {
    type Error = (); 

    fn try_from(set: u32) -> Result<Self, Self::Error> {
        if (set & 0xFE000000) != 0 {
            return Err(())
        }

        Ok(SetOfEvent{ set })
    }
}

// TODO: a method to check if an EventSet contains a given Event
enum Event {
    KeyPress,
    KeyRelease,
    ButtonPress,
    ButtonRelease,
    EnterWindow,
    LeaveWindow,
    PointerMotion,
    PointerMotionHint,
    Button1Motion,
    Button2Motion,
    Button3Motion,
    Button4Motion,
    Button5Motion,
    ButtonMotion,
    KeymapState,
    Exposure,
    VisibilityChange,
    StructureNotify,
    ResizeRedirect,
    SubstructureNotify,
    SubstructureRedirect,
    FocusChange,
    PropertyChange,
    ColormapChange,
    OwnerGrabButton
}

/*
     0x00000001     KeyPress
     0x00000002     KeyRelease
     0x00000004     ButtonPress
     0x00000008     ButtonRelease
     0x00000010     EnterWindow
     0x00000020     LeaveWindow
     0x00000040     PointerMotion
     0x00000080     PointerMotionHint
     0x00000100     Button1Motion
     0x00000200     Button2Motion
     0x00000400     Button3Motion
     0x00000800     Button4Motion
     0x00001000     Button5Motion
     0x00002000     ButtonMotion
     0x00004000     KeymapState
     0x00008000     Exposure
     0x00010000     VisibilityChange
     0x00020000     StructureNotify
     0x00040000     ResizeRedirect
     0x00080000     SubstructureNotify
     0x00100000     SubstructureRedirect
     0x00200000     FocusChange
     0x00400000     PropertyChange
     0x00800000     ColormapChange
     0x01000000     OwnerGrabButton
     0xFE000000     unused but must be zero
*/

/// sn unsisigned 8-bit value between 8 and 255 inclusive
type KeyCode = u8;
