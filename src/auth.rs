use std::path::Path;
use std::io::Read;
use std::io;

/// auth_token := family address number name data
#[derive(Debug)]
pub struct XAuthToken {
    pub family: u16,
    pub address: Vec::<u8>,
    pub number: Vec::<u8>,
    pub name: Vec::<u8>,
    pub data: Vec::<u8>,
}

/// read a .Xauthority file and return a vec of all tokens
/// TODO: maybe return an iterator
/// the Xauthority file contains concatenated auth tokens, comprised of msb-first 16-bit
/// integers and binary strings comprised of a 16-bit int length indicator followed by the
/// corresponding number of bytes.
/// Xauthority := auth_token ...
/// auth_token := family address number name data
/// family := u16
/// address, number, name, data: string
/// u16 := high_byte low_byte
/// string := length byte[length]
/// length := u16
pub fn read_auth() -> Vec<XAuthToken> {
    let authfilename = match std::env::var("XAUTHORITY") {
        Ok(s) => Box::<Path>::from(Path::new(&s)),
        Err(std::env::VarError::NotPresent) => match std::env::var("HOME") {
            Ok(s) => Box::<Path>::from((Path::new(&s)).join(".Xauthority")),
            Err(_) => todo!("no HOME and no XAUTHORITY set, don't know how to guess yet"),
        },
        Err(e) => todo!("handle env var error {:?}", e),
    };

    let mut f = std::fs::File::open(&authfilename).expect(
        &format!("couldn't open Xauthority file '{}'", authfilename.display())
    );
    let mut xauthority = Vec::<XAuthToken>::new();
    // TODO: while we can read a `family` value, construct an XAuthToken and extend xauthority
    let mut intbuf: [u8; 2] = [0, 0];
    loop {
        if let Err(e) = f.read_exact(&mut intbuf) {
            if e.kind() == io::ErrorKind::UnexpectedEof {
                break;
            }
            unimplemented!("don't know how to handle error: {:?}", e);
        };

        let family = u16::from_le_bytes(intbuf);

        macro_rules! read_string {
            ( $name:ident ) => {
                f.read_exact(&mut intbuf).expect(&format!(
                        "couldn't read length of string '{}'", stringify!($name)
                ));
                let length = u16::from_be_bytes(intbuf) as usize;
                let mut $name: Vec<u8> = Vec::with_capacity(length);
                $name.resize(length, 0);
                f.read_exact(&mut $name[..length])
                    .expect("couldn't read address");
            }
        }

        read_string!(address);
        read_string!(display_number);
        read_string!(name);
        read_string!(data);

        xauthority.push(XAuthToken {
            family: family,
            address: address,
            number: display_number,
            name: name,
            data: data,
        });
    }

    return xauthority;
}
