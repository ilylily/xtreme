/// functions and types for managing x displays, including opening them from the `DISPLAY`
/// environment variable or from a `DISPLAY`-like string
pub mod display;

/// for creating graphics contexts and drawing shapes
pub mod graphics;

/// functions for creating, managing, and cleaning up windows
pub mod window;

mod error_macros;
mod serides;

// TODO: publish this
mod auth;
