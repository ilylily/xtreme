macro_rules! wrap_in {
    ( $top:ident, $called:path, $bottom:path ) => {
        impl From<$bottom> for $top {
            fn from(e: $bottom) -> $top {
                $called(e)
            }
        }
    };
}

/// grab an enum and rewrite it, adding [From] implementations for each wrapped type
macro_rules! wrapper_err {
    ( $( #[$docs:meta] )* $pub:vis enum $enum:ident { $( $( #[$item_docs:meta] )* $item:ident $( ($inner:path) )? ),* $(,)? }  ) => {
        #[derive(Debug)]
        $( #[$docs] )*
        $pub enum $enum {
            $( $( #[$item_docs] )* $item $( ( $inner ) )? ),*
        }

        // only `[wrap_in]!` `$enum` those `$item`s which contain an `$inner`
        $( $( crate::error_macros::wrap_in!($enum, $enum::$item, $inner); )? )*
    }
}

pub(crate) use wrap_in;
pub(crate) use wrapper_err;
