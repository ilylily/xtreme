use std::sync::Arc;

use crate::display::XDisplay;

use crate::serides::seri::nb;
use crate::serides::bitmask_hash;

#[derive(Debug)]
pub struct Window {
    display: Arc<XDisplay>,
    id: u32,
}

bitmask_hash!(
    pub bitmask WindowAttributes WindowAttrKeys WINDOW_ATTR_KEYS_COUNT {
        BackgroundPixmap,
        BackgroundPixel,
        BorderPixmap,
        BorderPixel,
        BitGravity,
        WinGravity,
        BackingStore,
        BackingPlanes,
        BackingPixel,
        OverrideRedirect,
        SaveUnder,
        EventMask,
        DoNotPropagateMask,
        Colormap,
        Cursor,
    }
);

/// TODO: rewrite. this is scaffolding, to put it politely
pub fn create(display: &Arc<XDisplay>) -> Window {
    let display = display.clone();
    let root_id = display.server_info.as_ref().unwrap().get_screen(0).unwrap().root;
    let mut socket = display.socket.write().unwrap();

    let window = from_id(display.clone(), display.generate_res_id());
    let mut buf = Vec::<u8>::with_capacity(32); // TODO: minimum length of createwindow
    nb!(buf, 1u8); // opcode 1 is create window
    nb!(buf, 24u8); // depth
    nb!(buf, 9u16); // request length = 8 + number of value bits set
    nb!(buf, window.id);
    nb!(buf, root_id);
    nb!(buf, 0u16); // x
    nb!(buf, 0u16); // y
    nb!(buf, 640u16); // width
    nb!(buf, 480u16); // height
    nb!(buf, 5u16); // border width?
    nb!(buf, 1u16); // class. 1 is input/output, the normal window
    nb!(buf, 0u32); // visualid. 0 is copy from parent
    let mut mask = WindowAttributes::new();
    mask.insert(WindowAttrKeys::BackgroundPixel, 0xeff1f5u32);
    nb!(buf, mask.mask());
    for value in mask.values() {
        nb!(buf, value);
    }

    // TODO: move all socket i/o into display itself. just accept a buffer to write and provide a
    // queue of decoded messages, probably
    socket.write_all(&buf[..]).expect("bad write");
    socket.flush().unwrap();
    return window
}

impl Window {
    pub fn id(&self) -> u32 {
        self.id
    }

    /// make a window visible
    pub fn map(&mut self) {
        let mut socket = self.display.socket.write().unwrap();
        let mut buf = Vec::<u8>::with_capacity(32); // TODO: minimum length of createwindow
        nb!(buf, 8u8); // opcode 8 is map window
        nb!(buf, 0u8); // unused
        nb!(buf, 2u16); // length is 2 (length is in 4-byte units, so this plus last two plus u32)
        nb!(buf, self.id); // window id as u32
        socket.write_all(&buf[..]).expect("bad write");
        socket.flush().unwrap();
        //let mut buf = [0u8; 128];
        //let n = socket.read(&mut buf[..]).unwrap();
        //println!("{}, {:?}", n, &buf[0..n]);
    }

    /// set an area to bg colour
    pub fn clear(&mut self) {
        let mut socket = self.display.socket.write().unwrap();
        let mut buf = Vec::<u8>::with_capacity(16);
        nb!(buf, 61u8);
        nb!(buf, 0u8);
        nb!(buf, 4u16);
        nb!(buf, self.id);
        nb!(buf, 0u16);
        nb!(buf, 0u16);
        nb!(buf, 640i16);
        nb!(buf, 480i16);
        socket.write_all(&buf[..]).expect("bad write");
        socket.flush().unwrap();
    }

    pub fn close(&mut self) /* -> Result<...?> */ {
        unimplemented!("don't know how to close a window yet");
    }
}

pub fn from_id(display: Arc<XDisplay>, id: u32) -> Window {
    Window { display, id }
}
