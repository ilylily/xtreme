pub(crate) mod des {
    /// read $t worth of bytes from socket and return them as a $t
    macro_rules! bn {( $socket:expr, $t:ty ) => {
        {
            let mut buffer = [0u8; std::mem::size_of::<$t>()];
            $socket.read_exact(&mut buffer[0..std::mem::size_of::<$t>()])
                .expect(&format!("couldn't parse data from X server as type {}", stringify!($t)));
            <$t>::from_le_bytes(buffer)
        }
    }}

    macro_rules! deserialize_to{
        ( $socket:expr, $name:ident: $t:ty, $len:expr ) => {
            let $name: $t = crate::serides::des::Deserialize::deserialize($socket, $len);
        };
        ( $socket:expr, $name:ident: $t:ty ) => { let $name = crate::serides::des::bn!($socket, $t); };
    }

    /// take a socket, and a list of names and types. from the socket, read in order each type to a
    /// new variable with the corresponding name
    macro_rules! deserialize{
        ( $socket:expr, $($name:ident: $t:ty $(= $len:expr)?),* ) => {
            $(crate::serides::des::deserialize_to!($socket, $name: $t $(, $len)?);)*
        }
    }

    /// [bn] for a slice instead of a stream
    macro_rules! bns {( $data:expr, $cursor:expr, $t:ty ) => {
        {
            let len = std::mem::size_of::<$t>();
            let slice = &$data[$cursor..$cursor + len];
            $cursor += len;
            <$t>::from_le_bytes(slice.try_into().unwrap())
        }
    }}

    macro_rules! deserialize_slice_to {
        ( $data:expr, $cursor:expr, $name:ident: $t:ty, $len:expr ) => {
            let $name: $t = crate::serides::des::Deserialize::deserialize_slice($data, $cursor, $len);
            $cursor += $len as usize;
        };
        ( $data:expr, $cursor:expr, $name:ident: $t:ty ) => {
            let $name = crate::serides::des::bns!($data, $cursor, $t);
        };
    }

    macro_rules! deserialize_slice {
        ( $data:expr, $cursor:expr, $($name:ident: $t:ty $(= $len:expr)?),* ) => {
            $(crate::serides::des::deserialize_slice_to!($data, $cursor, $name: $t $(, $len)?);)*
        }
    }

    pub(crate) use deserialize;
    pub(crate) use deserialize_to;
    pub(crate) use deserialize_slice;
    pub(crate) use deserialize_slice_to;
    //pub(crate) use bs;
    pub(crate) use bn;
    pub(crate) use bns;

    use std::io::Read;
    use crate::display::XConnection;
    pub(crate) trait Deserialize {
        fn deserialize(socket: &mut Box<impl XConnection + ?Sized>, len: impl Into<usize>) -> Self;
        fn deserialize_slice(data: &[u8], cursor: usize, len: impl Into<usize>) -> Self;
    }

    impl Deserialize for String {
        fn deserialize(socket: &mut Box<impl XConnection + ?Sized>, len: impl Into<usize>) -> Self {
            let len: usize = len.into();
            let mut buffer = Vec::with_capacity(len);
            buffer.resize(len, 0u8);
            socket.read_exact(&mut buffer[..]).unwrap();
            String::from_utf8(buffer).expect("couldn't parse server response as utf-8 string")
        }

        fn deserialize_slice(data: &[u8], cursor: usize, len: impl Into<usize>) -> Self {
            let buffer = Vec::from(&data[cursor..cursor+len.into()]);
            String::from_utf8(buffer).expect("couldn't parse server response as utf-8 string")
        }
    }

    impl Deserialize for Vec::<u8> {
        fn deserialize(socket: &mut Box<impl XConnection + ?Sized>, len: impl Into<usize>) -> Self {
            let len: usize = len.into();
            let mut buffer = Vec::with_capacity(len);
            buffer.resize(len, 0u8);
            socket.read_exact(&mut buffer[..]).unwrap();
            return buffer
        }

        fn deserialize_slice(data: &[u8], cursor: usize, len: impl Into<usize>) -> Self {
            Vec::from(&data[cursor..cursor+len.into()])
        }
    }
}

pub(crate) mod seri {
    /// put a number as BE bytes into buffer
    /// nb is short for "number -> buffer"
    macro_rules! nb{
        ( $buf:expr, $no:expr ) => {
            $buf.extend_from_slice(&$no.to_le_bytes());
        }
    }

    pub(crate) use nb;

    pub trait Serialize {
        fn serialize_to(&self, buf: &mut Vec::<u8>);

        fn seri_bufsize(&self) -> usize;

        fn serialize(&self) -> Vec::<u8> {
            let mut buf = Vec::with_capacity(self.seri_bufsize());
            self.serialize_to(&mut buf);
            return buf;
        }
    }
}

macro_rules! bitmask_hash {
    ($vis:vis bitmask $name:ident $keys:ident $count:ident { $($key:ident),*$(,)? }) => {
        #[doc = "A [HashMap](std::collections::HashMap) wrapper keyed by ["]
        #[doc = stringify!($keys)]
        #[doc = "] with some helper functions for communicating with an X server"]
        $vis struct $name {
            hash: std::collections::HashMap<u32, u32>,
        }

        #[doc = "Possible values which can be represented by the bitmask hash ["]
        #[doc = stringify!($name)]
        #[doc = "]"]
        #[derive(Clone, Copy)]
        #[repr(u32)]
        $vis enum $keys {
            $($key),*,
        }

        #[doc = "The number of keys in ["]
        #[doc = stringify!($keys)]
        #[doc = "]"]
        $vis const $count: u32 = {
            #[allow(dead_code)]
            enum CountKeys { $($key),*, __SeriLastKey }
            CountKeys::__SeriLastKey as u32
        };

        impl $name {
            $vis fn new() -> Self {
                return $name {
                    hash: std::collections::HashMap::new(),
                }
            }

            fn mask(&self) -> u32 {
                return self.hash.keys().fold(0, |acc, item| {
                    acc | (1 << (*item as u32))
                });
            }

            fn values(&self) -> Vec<u32> {
                let mut vec = Vec::with_capacity(self.hash.len());
                for i in 1..$count as u32 {
                    if self.hash.contains_key(&i) {
                        vec.push(self.hash.get(&i).unwrap().clone());
                    }
                }
                vec
            }

            $vis fn insert(&mut self, key: $keys, value: u32) -> Option<u32> {
                self.hash.insert(key as u32, value)
            }

            $vis fn contains_key(&self, key: $keys) -> bool {
                self.hash.contains_key(&(key as u32))
            }

            $vis fn get(&self, key: $keys) -> Option<&u32> {
                self.hash.get(&(key as u32))
            }
        }
    }
}

pub(crate) use bitmask_hash;
