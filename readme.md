# xtreme!! 🧑‍🎤

this aims to be a plain x11 client library, implemented according to the protocol at https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html as required by other software i write

i intend to not depend on other crates


## todo

- [x] connect to an x server and speak an amount of x11 protocol with it
- [x] authenticate using the `MIT-MAGIC-COOKIE-1` from `$HOME/.Xauthority`
- [x] open a window
- [x] draw a rectangle
- [x] draw a line
- [x] draw some text
- [x] draw all these things with colour
- [ ] close a window
- [ ] handle mouse events
- [ ] handle keyboard events
