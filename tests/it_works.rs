#[test]
fn just_open() {
    let conn = xtreme::display::open().unwrap();
    println!("{}", conn.remote().unwrap());
    conn.close();
}

fn open_display(display: &str) {
    let conn = xtreme::display::open_display(display).unwrap();
    println!("{}", conn.remote().unwrap());
    conn.close();
}

#[test]
fn with_addr() {
    // TODO: most people aren't on my ssh connection
    open_display("localhost:10.0");
}

#[test]
fn with_numeric_addr() {
    // TODO: most people aren't on my ssh connection
    open_display("127.0.0.1:10");
}

#[cfg(feature = "unix")]
#[test]
fn with_local() {
    // TODO: what if they're not on :0
    open_display(":0");
}

#[cfg(feature = "unix")]
#[test]
fn with_local_with_dot() {
    // TODO: what if they're not on :0
    open_display(":0.0");
}
