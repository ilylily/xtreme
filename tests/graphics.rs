use xtreme::graphics::{GCOptions, GCOptionsKey};

#[test]
fn geometry() {
    let xdisplay = xtreme::display::open().unwrap();
    let mut window = xtreme::window::create(&xdisplay);

    let mut options = GCOptions::new();
    options.insert(GCOptionsKey::Foreground, 0x7287fd);
    options.insert(GCOptionsKey::LineWidth, 10);
    let gc = xtreme::graphics::GraphicsContext::create(&xdisplay, &window, options);

    window.map();
    window.clear();
    std::thread::sleep(std::time::Duration::from_millis(200));
    gc.rectangle(0x14, 0x28, 0x96, 0x64);
    gc.line(0x14, 0x28, 0x14 + 0x96, 0x28 + 0x64);
    println!("window id {:#x?}", window.id());
    std::thread::sleep(std::time::Duration::from_millis(300));
    //window.close();
}

#[test]
fn text() {
    let xdisplay = xtreme::display::open().unwrap();
    let mut window = xtreme::window::create(&xdisplay);

    let mut options = GCOptions::new();
    options.insert(GCOptionsKey::Foreground, 0x40a02b);
    let gc1 = xtreme::graphics::GraphicsContext::create(&xdisplay, &window, options);

    let mut options = GCOptions::new();
    options.insert(GCOptionsKey::Foreground, 0x04a5e5);
    let gc2 = xtreme::graphics::GraphicsContext::create(&xdisplay, &window, options);

    window.map();
    window.clear();
    std::thread::sleep(std::time::Duration::from_millis(200));
    gc1.text("everything's cool :)");
    gc2.text("                     and nothing is wrong :D");
    std::thread::sleep(std::time::Duration::from_millis(300));
    //window.close();
}
